import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Redirect, useHistory, Link } from 'react-router-dom';

import UserContext from '../UserContext';


export default function Login() {

    const history = useHistory();

    // State variables for user inputs
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    // Button states
    const [isActive, setIsActive] = useState(true);

    // UserContext variables
    const { user, setUser } = useContext(UserContext);


    useEffect(() => {
        if(email !== '' && password !== '') {
            setIsActive(false);
        }else {
            setIsActive(true);
        }
    }, [email, password]);

    function loginUser(e) {
        e.preventDefault();

        fetch('https://radiant-garden-68449.herokuapp.com/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {

            if(data.accessToken !== undefined) {
                localStorage.setItem('accessToken', data.accessToken);

                setUser({ 
                    accessToken: data.accessToken,
                });

                Swal.fire({
                    title: "Success",
                    icon: 'success',
                    text: "You are now logged in"
                });

                setEmail('');
                setPassword('');

                // Get details from the access token
                fetch('https://radiant-garden-68449.herokuapp.com/users/details', {
                    headers: {
                        Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                    }
                })
                .then(res => res.json())
                .then(data => {
                    console.log(data);

                    localStorage.setItem('fullName', data.fullName);
                    localStorage.setItem('username', data.username);
                    localStorage.setItem('email', data.email);
                    localStorage.setItem('isAdmin', data.isAdmin);

                    setUser({
                        fullName: data.fullName,
                        username: data.username,
                        email: data.email,
                        isAdmin: data.isAdmin
                    });
                });

                // Redirect page dependent on isAdmin value
                if(data.isAdmin) {
                    history.push('/products');
                }else {
                    history.push('/');
                }


            }else {
                Swal.fire({
                    title: "Log in failed",
                    icon: 'error',
                    text: 'Something went wrong. Check your credentials.'
                })
            }
        });
    }

    if(user.accessToken !== null) {
        return <Redirect to="/" />
    }

    return (
        <Form onSubmit={e => loginUser(e)} className="bg-dark text-light p-4">
            <h1>Login</h1>
            <hr color="white"/>
            <Form.Group controlId="email">
                <Form.Label>Email:</Form.Label>
                <Form.Control 
                    type="email"
                    placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password:</Form.Label>
                <Form.Control 
                    type="password"
                    placeholder="Enter password"
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            <br />
            <Button variant="info" type="submit" disabled={isActive} className="btn-block">Login</Button>
            <span className="text-center">
                <div>
                    Don't have an account yet? <Link to="/register">Click here</Link> to register.
                </div>
            </span>
        </Form>
    );
}