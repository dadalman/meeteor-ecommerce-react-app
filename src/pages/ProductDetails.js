import React, { useState, useEffect } from 'react';
import { Row, Col, Card, Button, Form } from 'react-bootstrap';
import { useParams, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

import AddToCart from '../components/AddToCart';


export default function ProductDetails() {

    const { id } = useParams();

    console.log(`Product ID: ${id}`);

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);
    const [amount, setAmount] = useState(1);
    const [disable, setDisable] = useState(false);
    
    useEffect(() => {

        if(amount <= 1) {
            setDisable(true);
        }else {
            setDisable(false);
        }


        fetch(`https://radiant-garden-68449.herokuapp.com/products/product/${id}`, {
            Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);            
        });

        if(amount <= 0) {
            setAmount(1);

            Swal.fire({
                title: "Invalid Amount",
                text: "Please enter an amount, no less than one (1)"
            })
        }

    }, [amount]);

    function addQuantity(e) {
        e.preventDefault();

        setAmount(amount + 1);
    }

    function subtractQuantity(e) {
        e.preventDefault();

        setAmount(amount - 1);
    }

    return (
        <>
            <Row>
                <Col>
                    <Card className="productDetails bg-dark text-light">
                        <Card.Body>
                            <Card.Title>
                                <h4>{name}</h4>
                            </Card.Title>
                            <Card.Text>
                                <span><strong>Description:</strong></span>
                                <br />
                                <span>{description}</span>
                                <br />
                                <span><strong>Price:</strong></span>
                                <br />
                                <span>&#8369; {price}</span>
                                <br />
                            </Card.Text>
                            
                            <Form>
                                <Form.Group controlId="quantity">
                                    <Form.Label>Quantity:</Form.Label>
                                    <span className="form-inline">
                                        <Button variant="info" onClick={e => subtractQuantity(e)} className="rounded-0" disabled={disable}>  -  </Button>
                                        <Form.Control
                                            type="number"
                                            value={amount}
                                            onChange={e => setAmount(e.target.value)}
                                            className="rounded-0"
                                            required
                                        />
                                        <Button variant="info" onClick={e => addQuantity(e)} className="rounded-0" > + </Button>
                                    </span>
                                </Form.Group>
                            </Form>

                            <AddToCart productId={id} name={name} price={price} totalAmount={amount} />
                            
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
            

        </>
    );
}