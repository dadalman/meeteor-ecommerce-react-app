import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Redirect, useHistory, Link } from 'react-router-dom';

import UserContext from '../UserContext';


export default function Register() {
    const history = useHistory();

    // State variables
    const [fullName, setFullName] = useState('');
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [verifyPassword, setVerifyPassword] = useState('');
    
    // Enables and disables to button
    const [isActive, setIsActive] = useState(true);

    // UserContext variables
    const { user } = useContext(UserContext);

    useEffect(() => {
        // Checks user input
        if(fullName !== '' && username !== '' && email !== '' &&password !== '' && verifyPassword !== '' && password === verifyPassword) {
            setIsActive(false);
        }else {
            setIsActive(true);
        }
    }, [fullName, username, email, password, verifyPassword]);


    // Register/add user to the database
    function registerUser(e) {
        e.preventDefault();

        // Register route from the backend
        fetch('https://radiant-garden-68449.herokuapp.com/users/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                fullName: fullName,
                username: username,
                email: email,
                password: password,
                verifyPassword: verifyPassword
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            
            if(data) {
                // Success alert message
                Swal.fire({
                    title: "Success!",
                    icon: 'success',
                    text: "You have successfully registered"
                });

                history.push("/login");
            }else {
                Swal.fire({
                    title: "Registration Failed!",
                    icon: 'error',
                    text: 'Something went wrong. Check your credentials.'
                });
            }
            
        })

        //Clear the fields after registration
        setFullName('');
        setUsername('');
        setEmail('');
        setPassword('');
        setVerifyPassword('');
    }
    
    if(user.accessToken !== null) {
        return <Redirect to="/" />
    }
    
    return (
        <>
            <Form onSubmit={e => registerUser(e)} className="bg-dark text-light p-4">
                <h1>Register</h1>
                <hr color="white"/>
                <Form.Group controlId="fullName">
                    <Form.Label>Full Name:</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter full name"
                        value={fullName}
                        onChange={e => setFullName(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="username">
                    <Form.Label>Username:</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter username"
                        value={username}
                        onChange={e => setUsername(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="email">
                    <Form.Label>Email:</Form.Label>
                    <Form.Control
                        type="email"
                        placeholder="Enter email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="password">
                    <Form.Label>Password:</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Enter password"
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="verifyPassword">
                    <Form.Label>Verify Password:</Form.Label>
                    <Form.Control
                        type="password"
                        placeholder="Verify your password"
                        value={verifyPassword}
                        onChange={e => setVerifyPassword(e.target.value)}
                        required
                    />
                </Form.Group>
                <br />

                <Button variant="info" type="submit" className="btn-block" disabled={isActive}>Register</Button>
                <span className="text-center">
                    <div>
                        Already have an account? <Link to="login">Click here</Link> to log in.
                    </div>
                </span>
            </Form>
        </>
    );
}