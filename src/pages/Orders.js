import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';


import OrderData from '../components/OrderData';


export default function Orders() {

    const [productOrders, setProductOrders] = useState([]);
    const [orderInfo, setOrderInfo] = useState('');

    useEffect(() => {

        fetch(`https://radiant-garden-68449.herokuapp.com/users/myOrders`, {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setProductOrders(data.map((order, index) => {
                return (
                    <span key={order._id}>
                        <OrderData order={order} index={index} />
                    </span>
                );
            }))
        })
        
    }, []);
    

    return(
        <>
            <h1 className="text-center my-3" >Orders</h1>
            <hr />

            {/* <div className="bg-dark text-white p-5 border"><h2 className="text-center m-5">No orders placed yet! <Link to="/products">Start shopping.</Link></h2></div> */}

            {productOrders}
            
        </>

    );

}