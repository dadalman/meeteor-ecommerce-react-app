import React, { useState, useEffect } from 'react';
import { Tabs, Tab, Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';

import ToAdmin from '../components/ToAdmin';

export default function FindUser() {

    const [userId, setUserId] = useState('');
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [disable, setDisable] =useState(true);

    const [userInfo, setUserInfo] = useState([]);


    useEffect(() => {
        if(userId !== '' || username !== '' || email !== '') {
            setDisable(false);
        }else {
            setDisable(true);
        }
        }, [userId, username, email]);

/*         if(localStorage.getItem('findEmail') !== null) {
            userData("", "", localStorage.getItem('findEmail'))
        }

        if(localStorage.getItem('findUsername') !== null) {
            userData("", localStorage.getItem('findUsername'), "")
        }

        if(localStorage.getItem('findId') !== null) {
            userData(localStorage.getItem('findId'), "", "")
        } */

        function userData(userId, username, email) {

            fetch(`https://radiant-garden-68449.herokuapp.com/users/user`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                },
                body: JSON.stringify({
                    userId: userId,
                    username: username,
                    email: email
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if(data.length !== 0) {
                    console.log(data[0]._id)

                    setUserInfo(data.map((data) => {
                        return (
                            <Form key={data._id} className="border bg-dark text-light">
                                {/* {(userId !== '') ?
                                    localStorage.setItem('findId', data.userId)
                                    :
                                    (username !== '') ?
                                        localStorage.setItem('findUsername', data.username)
                                        :
                                        localStorage.setItem('findEmail', data.email)
                                } */}
                                <div className="m-5">
                                    <h1>User Information</h1>
                                    <hr color="grey"/>
                                    
                                    <h5><span className="">Full Name: </span>
                                    <div className="border rounded text-dark" id="infoContainer">
                                        <span><strong>{data.fullName}</strong></span>
                                    </div>
                                    </h5>
                                    <br/>

                                    <h5><span className="">Username: </span><br />
                                    <div className="border rounded text-dark" id="infoContainer">
                                        <span><strong>{data.username}</strong></span>
                                    </div>
                                    </h5>
                                    <br/>

                                    <h5><span className="">Email: </span><br />
                                    <div className="border rounded text-dark" id="infoContainer">
                                        <span><strong>{data.email}</strong></span>
                                    </div>
                                    </h5>
                                    <br/>

                                    <h5><span className="">User Type: </span><br />
                                    <div className="border rounded text-dark" id="infoContainer">
                                        {(data.isAdmin) ?
                                            <span className="text-success"><strong>Administrator</strong></span>
                                            :
                                            <span  className="text-info"><strong>Normal User</strong></span>
                                        }
                                    </div>
                                    </h5>
                                    <br/>

                                    <h5><span className="">Account Created On: </span><br />
                                    <div className="border rounded text-dark" id="infoContainer">
                                        <span><strong>{data.createdOn.slice(0,10)}</strong></span>
                                    </div>
                                    </h5>
                                    <br/>

                                    <ToAdmin userId={data._id} fullName={data.fullName} isAdmin={data.isAdmin}/>
                                </div>
                                
                            </Form>
                        );
                    }))
                    
                }else {
                    Swal.fire({
                        title: "No User Match!",
                        icon: 'error',
                        text: "Please see that you've entered the right credentials."
                    })
                }

            

            
        })
    }   
        
    return (
        <>  
            <div className="bg-dark text-light p-4">

            <Tabs 
                defaultActiveKey="userId" 
                transition={false} 
                id="uncontrolled-tab-example" 
                className="mb-3"
            >
                <Tab eventKey="userId" title="User ID">
                    <Form>
                        <span className="form-inline">
                            <Form.Control
                                type="text"
                                value={userId}
                                onChange={e => {setUserId(e.target.value)}}
                                placeholder="Enter user ID"
                                id="controlDash"
                            />
                            <Button variant="primary" id="buttonDash" className="mx-3" onClick={() => userData(userId, "", "")} disabled={disable}>Find</Button>
                        </span>
                    </Form>
                </Tab>
                <Tab eventKey="username" title="Username">
                    <Form>
                        <span className="form-inline">
                            <Form.Control
                                type="text"
                                value={username}
                                onChange={e => {setUsername(e.target.value)}}
                                placeholder="Enter username"
                                id="controlDash"
                            />
                            <Button variant="primary" id="buttonDash" className="mx-3" onClick={() => userData("", username, "")} disabled={disable}>Find</Button>
                        </span>
                    </Form>
                </Tab>
                <Tab eventKey="email" title="Email">
                    <Form>
                        <span className="form-inline">
                            <Form.Control
                                type="text"
                                value={email}
                                onChange={e => {setEmail(e.target.value)}}
                                placeholder="Enter email"
                                id="controlDash"
                            />
                            <Button variant="primary" id="buttonDash" className="mx-3" onClick={() => userData("", "", email)} disabled={disable}>Find</Button>
                        </span>
                    </Form>
                </Tab>
            </Tabs>
            <hr color="gray" />

            {userInfo}

            </div>
        </>
    );
    }