import React, { useEffect, useContext, useState } from 'react';
import { Table, Row } from 'react-bootstrap';

import UserContext from '../UserContext';

import Product from '../components/Product';
import UpdateButton from '../components/UpdateButton';
import DeleteButton from '../components/DeleteButton';
import ToFeature from '../components/ToFeature';


export default function Products() {
    
    const [adminProducts, setAdminProducts] = useState([]);
    const [allProducts, setAllProducts] = useState([]);

    const { user } = useContext(UserContext);
    
    useEffect(() => {
        if(user.isAdmin) {
            fetch('https://radiant-garden-68449.herokuapp.com/products/all', {
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                setAdminProducts(data.map(product => {
                    return (
                        <tr key={product._id}> 
                            <td>{product._id}</td>
                            <td>{product.name}</td>
                            <td>{product.description}</td>
                            <td>{product.price}</td>
                            <td className={product.isActive ? "text-success text-center" : "text-danger text-center"}>{product.isActive ? "Active" : "Inactive"}</td>
                            {(product.isFeatured) ?
                                <td className="text-success text-center">Yes</td>
                                :
                                <td className="text-danger text-center">No</td>
                            }
                            <td>
                                <span className="form-inline">
                                    <UpdateButton productId={product._id} />
                                    <DeleteButton productId={product._id} isActive={product.isActive}/>
                                    <ToFeature productId={product._id} feature={product.isFeatured} />
                                </span>
                            </td>
                        </tr>
                    )
                }))

            })
        }else {
            fetch('https://radiant-garden-68449.herokuapp.com/products/active', {
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                setAllProducts(data.map((product) => {
                    return (
                        <Product key={product._id} product={product} productId={product._id}/>
                    );
                }));
            });


        }
    }, []);

    if(user.isAdmin) {
        return (
            <>
                <Table striped bordered hover variant="dark">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Status</th>
                            <th>Featured</th>
                            <th className="text-center">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {adminProducts}
                    </tbody>
                </Table>
            </>
        );
    }else {
        return (
            <>
                <h1 className="text-center mt-3">Our Products</h1>
                <hr />
                <Row>
                    {allProducts}
                </Row>
                
            </>
        )
    }
    
}