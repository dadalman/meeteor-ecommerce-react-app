import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { Redirect } from 'react-router-dom';

import UserContext from '../UserContext';


export default function AddProduct() {

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);

    const [isActive, setIsActive] = useState(true);

    const { user } = useContext(UserContext);

    useEffect(() => {
        if(name !== '' && description !== '' && price !== 0 && price !== '') {
            setIsActive(false);
        }else {
            setIsActive(true);
        }
    }, [name, description, price]);

    function addProduct(e) {
        e.preventDefault();

        console.log(localStorage.getItem('accessToken'));

        fetch('https://radiant-garden-68449.herokuapp.com/products/add', {
            method: 'POST',
            headers: {
                'Content-Type': "application/json",
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data) {
                Swal.fire({
                    title: "Success!",
                    icon: 'success',
                    text: "You successfully added a product"
                });
            }else {
                Swal.fire({
                    title: "Product Creation Failed",
                    icon: 'error',
                    text: "You failed to add a product"
                });
            }
            
            setName('');
            setDescription('');
            setPrice(0);
        });
    }

    if(user.isAdmin !== true && user.accessToken !== null) {
        return <Redirect to="/" />
    }

    return (
        <div className="bg-dark text-light p-4">
            <Form onSubmit={e => addProduct(e)} >
                <Form.Group controlId="name">
                    <Form.Label>Product Name:</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter product name"
                        value={name}
                        onChange={e => setName(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="description">
                    <Form.Label>Description:</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter product description"
                        value={description}
                        onChange={e => setDescription(e.target.value)}
                        required
                    />
                </Form.Group>

                <Form.Group controlId="price">
                    <Form.Label>Price:</Form.Label>
                    <Form.Control
                        type="text"
                        placeholder="Enter product price"
                        value={price}
                        onChange={e => setPrice(e.target.value)}
                        required
                    />
                </Form.Group>

                <Button variant="primary" type="submit" disabled={isActive}>Add Product</Button>
            </Form>
        </div>
    );
}