import React, { useEffect, useState } from 'react';
import { Row, Col, Form } from 'react-bootstrap';


export default function UserDetails() {

    const [fullName, setFullName] = useState('');
    const [username, setUsername] = useState('');
    const [email, setEmail] = useState('');
    const [createdDate, setCreatedDate] = useState('');
    const [createdTime, setCreatedTime] = useState('');

    useEffect(() => {
        fetch('https://radiant-garden-68449.herokuapp.com/users/details', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setFullName(data.fullName);
            setUsername(data.username);
            setEmail(data.email);
            setCreatedDate(data.createdOn.slice(0, 10));
            setCreatedTime(data.createdOn.slice(11, 19))
        })
    }, [])

    

    return (
        <>
            <Row>
                <Col>
                    <Form className="border bg-dark text-light">
                        <div className="m-5">
                            <h1>Personal Information</h1>
                            <hr color="grey"/>
                            
                            <h5><span className="">Full Name: </span>
                            <div className="border rounded text-dark" id="infoContainer">
                                <span><strong>{fullName}</strong></span>
                            </div>
                            </h5>
                            <br/>
                            <h5><span className="">Username: </span><br />
                            <div className="border rounded text-dark" id="infoContainer">
                                <span><strong>{username}</strong></span>
                            </div>
                            </h5>
                            <br/>
                            <h5><span className="">Email: </span><br />
                            <div className="border rounded text-dark" id="infoContainer">
                                <span><strong>{email}</strong></span>
                            </div>
                            </h5>
                            <br/>
                            <h5><span className="">Date Created: </span><br />
                            <div className="border rounded text-dark" id="infoContainer">
                                <span><strong>{createdDate}</strong></span>
                            </div>
                            </h5>
                            <br/>
                        </div>
                    </Form>
                </Col>
            </Row>
            
            
        </>
    );
}