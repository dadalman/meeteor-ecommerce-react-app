import React, { useEffect, useState, useContext } from 'react';
import { Redirect } from 'react-router-dom';

import AllOrders from '../components/AllOrders';
import UserContext from '../UserContext';


export default function OrdersAdmin() {
    
    const { user } = useContext(UserContext);
    const [allOrders, setAllOrders] = useState([]);

    useEffect(() => {
        fetch(`https://radiant-garden-68449.herokuapp.com/users/allOrders`, { 
            headers: {
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setAllOrders(data.map((order, index) => {
                return(
                    <span key={order._id}>
                        <AllOrders order={order} index={index}/>
                    </span>
                );
            }));
        })
    }, []);

    if(user.isAdmin !== true && user.accessToken !== null) {
        return <Redirect to="/" />
    }

    return (
        <>
            {allOrders}
        </>
    );
}