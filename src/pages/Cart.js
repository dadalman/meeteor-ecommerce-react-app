import React, { useState, useEffect, useContext } from 'react';
import { Table, Row, Col, Button, Form } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import Checkout from '../components/Checkout';
import UserContext from '../UserContext';
import RemoveProduct from '../components/RemoveProduct';
import ProductQuantity from '../components/ProductQuantity';


export default function Cart() {

    const [total, setTotal] = useState(0);
    const [products, setProducts] = useState([]);

    const { setOrders } = useContext(UserContext);


    var totalPrice = 0;


    
    useEffect(() => {
 

        fetch('https://radiant-garden-68449.herokuapp.com/users/myCart', {
        headers: { Authorization: `Bearer ${localStorage.getItem('accessToken') }`}
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            console.log(data.length === 0)

            let stringOrders = JSON.stringify(data)
            

            setOrders(stringOrders);

            
            setProducts(data.map(product => {
                return (
                    <tr key={product._id}>
                        <td>{product.name}</td>
                        <td className="text-center">&#8369;{product.price}</td>
                        <td className="text-center form-inline">
                            {product.totalAmount}
                            {/* <ProductQuantity totalAmount={product.totalAmount} /> */}
                        </td>
                        <td className="text-center">&#8369;{subTotal(product.price, product.totalAmount)}</td>
                        <td className="text-center"><RemoveProduct productId={product.productId} /></td>
                    </tr>
                );
            }))
        })

    }, []); 

    function subTotal(price, totalAmount) {
        let subTotal = price * totalAmount;
        
        totalPrice = totalPrice + subTotal;

        setTotal(totalPrice);

        return subTotal;
    }


    return (
        <>
        <Row>
            <Col>
                <h1 className="text-center my-3" >Your Shopping Cart</h1>
                <hr />
                {(products.length === 0) ?
                    <div className="bg-dark text-white p-5 border"><h2 className="text-center m-5">Your cart is empty! <Link to="/products">Start shopping.</Link></h2></div>
                    :
                    <Table striped bordered hover variant="dark">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th className="text-center">Price</th>
                                <th>Quantity</th>
                                <th className="text-center">SubTotal</th>
                            </tr>
                        </thead>
                        <tbody>
                            {products}
                            <tr>
                                <td colSpan="3" className="text-center"><Checkout total={total}/></td>
                                <td colSpan="2" className="h2">Total: &#8369;{total}</td>
                            </tr>
                        </tbody>
                     </Table>
                   
                     }
                </Col>
            </Row>
        </>
    );
}