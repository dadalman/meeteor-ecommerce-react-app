import React from 'react';
import { Nav } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';

export default function notFound() {
    return (
        <>
        <h1 align="center">Page Not Found/Error 404</h1>
        <h2 align="center">Go back to the <Nav.Link as={NavLink} to="/">home page</Nav.Link></h2>
        </>
    )
}