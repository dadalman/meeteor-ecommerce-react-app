import React, { useState } from 'react';
import { Button } from 'react-bootstrap';

import Products from './Products';
import AddProduct from './AddProduct';
import OrdersAdmin from './OrdersAdmin.js'
import FindUser from './FindUser.js';


export default function AdminDashboard() {

    const [content, setContent] = useState(localStorage.getItem('content'));

    function show(info) {
        setContent(info);
    }

    
    return (
        <>
            <h1 className="text-center mb-3 mt-3">Admin Dashboard</h1>
            {(content === "add") ? 
                <div>
                    <div className="mb-4 text-center" >
                        {localStorage.setItem("content", "add")}
                        <Button variant="dark" className="mx-1" id="buttonDash" onClick={() => show("products")}>Show All Products</Button>
                        <Button variant="dark" className="mx-1" id="buttonDash" onClick={() => show("add")} disabled>Add New Product</Button>
                        <Button variant="dark" className="mx-1" id="buttonDash" onClick={() => show("orders")}>Show User Orders</Button>
                        <Button variant="dark" className="mx-1" id="buttonDash" onClick={() => show("find")}>Find User</Button>
                    </div>
                    <AddProduct />
                </div>
                :
                (content === "orders") ? 
                    <div>
                        <div className="mb-4 text-center" >
                            {localStorage.setItem("content", "orders")}
                            <Button variant="dark" className="mx-1" id="buttonDash" onClick={() => show("products")}>Show All Products</Button>
                            <Button variant="dark" className="mx-1" id="buttonDash" onClick={() => show("add")}>Add New Product</Button>
                            <Button variant="dark" className="mx-1" id="buttonDash" onClick={() => show("orders")} disabled>Show User Orders</Button>
                            <Button variant="dark" className="mx-1" id="buttonDash" onClick={() => show("find")}>Find User</Button>
                        </div>
                        <OrdersAdmin />
                    </div>
                    :
                    (content === "find") ?
                        <div>
                            <div className="mb-4 text-center" >
                                {localStorage.setItem("content", "find")}
                                <Button variant="dark" className="mx-1" id="buttonDash" onClick={() => show("products")}>Show All Products</Button>
                                <Button variant="dark" className="mx-1" id="buttonDash" onClick={() => show("add")}>Add New Product</Button>
                                <Button variant="dark" className="mx-1" id="buttonDash" onClick={() => show("orders")}>Show User Orders</Button>
                                <Button variant="dark" className="mx-1" id="buttonDash" onClick={() => show("find")} disabled>Find User</Button>
                            </div>
                            <FindUser />
                        </div>
                        :
                        <div>
                            <div className="mb-4 text-center" >
                                {localStorage.setItem("content", "products")}
                                <Button variant="dark" className="mx-1" id="buttonDash" onClick={() => show("products")} disabled>Show All Products</Button>
                                <Button variant="dark" className="mx-1" id="buttonDash" onClick={() => show("add")}>Add New Product</Button>
                                <Button variant="dark" className="mx-1" id="buttonDash" onClick={() => show("orders")}>Show User Orders</Button>
                                <Button variant="dark" className="mx-1" id="buttonDash" onClick={() => show("find")}>Find User</Button>
                            </div>
                            <Products />
                        </div>
            }
            
        </>
    );
}