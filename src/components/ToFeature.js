import React from 'react';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom';
import { Button } from 'react-bootstrap';


export default function ToFeature({ productId, feature }) {

    console.log(productId, feature);

    const history = useHistory();

    function toFeature(isFeatured) {

        console.log(isFeatured)

        fetch(`https://radiant-garden-68449.herokuapp.com/products/featured/${productId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify({
                isFeatured: isFeatured
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data) {
                (isFeatured) ?
                    Swal.fire({
                        title: "Product is now featured!",
                        icon: "success"
                    })
                    .then(() => history.go(0))
                    :
                    Swal.fire({
                        title: "Product is now removed from the featured products!",
                        icon: "info"
                    })
                    .then(() => history.go(0))
            }else {
                Swal.fire({
                    title: "Uh-oh! Something went wrong!",
                    icon: 'error',
                    body: "Please check your internet connection and refresh the page."
                })
            }
        })
    }


    return (
        <>
            {(feature) ?
                <Button variant="outline-danger" className="btn-block" onClick={() => {toFeature(false)}}>Unfeature</Button>
                :
                <Button variant="outline-success" className="btn-block" onClick={() => {toFeature(true)}}>Feature</Button>
            }
        </>
    );

}