import React, { useContext } from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom';

import UserContext from '../UserContext';


export default function Checkout({ total }) {

    const history = useHistory();
    const { user, orders } = useContext(UserContext);

    let products = eval(orders);

    function checkout() {

        console.log(products);

        fetch(`https://radiant-garden-68449.herokuapp.com/users/order`, {
            method: 'POST',
            headers: { 
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify({
                fullName: user.fullName,
                username: user.username,
                email: user.email,
                products: products,
                totalPrice: total,
                status: "pending"
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data) {
                Swal.fire({
                    title: "Order success!",
                    icon: 'success'
                })
                .then(
                    fetch('https://radiant-garden-68449.herokuapp.com/users/remove', {
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json',
                            Authorization: `Bearer ${localStorage.getItem('accessToken')}`
                        },
                        body: JSON.stringify({
                            productId: 'all'
                        })
                    })
                    .then(res => res.json())
                    .then(data => {
                        console.log(data);

                        
                    })
                    
                )

                .then(() => history.go(0));
            }else {
                Swal.fire({
                    title: "Something went wrong!",
                    icon: 'error',
                    text: "Try to check your internet connection or refresh your page."
                })
            }
        })
    }

    return(
        <>
            <Button variant="info" className="btn-block" onClick={checkout}>Checkout</Button>
        </>
    );

}