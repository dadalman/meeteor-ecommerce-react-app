import React from 'react';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom';
import { Button } from 'react-bootstrap';


export default function RemoveProduct({ productId }) {

    const history = useHistory();

    function removeProduct() {
        fetch('https://radiant-garden-68449.herokuapp.com/users/remove', {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify({
                productId: productId
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data) {
                Swal.fire({
                    title: "Product successfully removed",
                    icon: 'success'
                })
                .then(() => history.go(0));
            }else {
                Swal.fire({
                    title: "Something went wrong!",
                    icon: 'error',
                    text: "Try to check your internet connection or refresh your page."
                })
            }
        })
    };

    return(
        <>
            <Button variant="danger" onClick={removeProduct}>Remove</Button>
        </>
    );
}