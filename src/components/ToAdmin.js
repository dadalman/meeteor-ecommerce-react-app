import React, { useState } from 'react';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

import Swal from 'sweetalert2';


export default function ToAdmin({ userId, fullName, isAdmin }) {

    const history = useHistory();
    const [admin, setAdmin] = useState(isAdmin);

    function toAdmin() {
        fetch(`https://radiant-garden-68449.herokuapp.com/users/toAdmin/${userId}`, {
            method: 'PUT',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data) {
                Swal.fire({
                    title:  `${fullName} is now an admin!`,
                    icon: "success"
                })
                .then(() => history.go(0))
            }else {
                Swal.fire({
                    title: "Uh-oh! Something went wrong!",
                    icon: 'error',
                    body: "Please check your internet connection and refresh the page."
                })
            }
        })
    }


    return (
        <>
            <Button variant="primary" onClick={toAdmin}>Set to Admin</Button>
        </>
    );
}