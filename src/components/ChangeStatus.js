import React, { useEffect, useState } from 'react';
import Swal from 'sweetalert2';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';


export default function ChangeStatus({ userId, orderId, status }) {

    const history = useHistory();
    const [disable, setDisable] =  useState(false);


    useEffect(() => {
        if(status === "delivered") {
            setDisable(true);
        }else {
            setDisable(false);
        }
    }, [])

    function changeStatus( userId, orderId, status ) {
        fetch(`https://radiant-garden-68449.herokuapp.com/users/status`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        },
        body: JSON.stringify({
            userId: userId,
            status: status,
            orderId: orderId
        })
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);

        Swal.fire({
            title: "Status changed!",
            icon: "success"
        })
        .then(() => history.go(0))
    })
    }

    return (
        <>
            {(status === "pending") ?
                <Button variant="success"  onClick={() => changeStatus(userId, orderId, "approved")}>Approve</Button>
                :
                <Button variant="success"  onClick={() => changeStatus(userId, orderId, "delivered")} disabled={disable}>Delivered</Button>
                }
            
            {/* <Button variant="success" onClick={changeStatus(userId, orderId, "approved")}>Approve</Button>
            <Button variant="success" onClick={changeStatus(userId, orderId, "approved")}>Approve</Button> */}
            
        </>
    );
}