import React, { useState, useEffect } from "react";
import { Button, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';


export default function ProductQuantity({ totalAmount }) {

    const [amount, setAmount] = useState(totalAmount);
    const [disable, setDisable] = useState(false);

    function addQuantity(e) {
        e.preventDefault();

        setAmount(amount + 1);
    }

    function subtractQuantity(e) {
        e.preventDefault();

        setAmount(amount - 1);
    }

    useEffect(() => {
        if(amount <= 1) {
            setDisable(true);
        }else {
            setDisable(false);
        }

        if(amount <= 0) {
            setAmount(1);

            Swal.fire({
                title: "Invalid Amount",
                text: "Please enter an amount, no less than one (1)"
            })
        }

    }, [amount]);

    return (
        <>
            <Button variant="info" onClick={e => subtractQuantity(e)} className="rounded-0" disabled={disable}>  -  </Button>
                <Form.Control
                    type="number"
                    value={amount}
                    onChange={e => setAmount(e.target.value)}
                    className="rounded-0"
                    required
                />
            <Button variant="info" onClick={e => addQuantity(e)} className="rounded-0" > + </Button>
        </>
    );
}