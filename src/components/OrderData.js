import React, { useState } from 'react';
import { Collapse, Row, Col } from 'react-bootstrap';

import CancelOrder from './CancelOrder';
import ReceiveOrder from './ReceiveOrder';


export default function OrderData({ order, index }) {

    const [open, setOpen] = useState(false);

    return (
        <>
            <div className="border boder-dark">
                <span
                    onClick={() => setOpen(!open)}
                    aria-controls="example-fade-text"
                    aria-expanded={open}
                    className="bg-dark text-light border border-light btn-block rounded-0 text-left"
                > 
                    <div className="m-2">Order #{index + 1} - Purchased on: {order.purchasedOn.slice(4, 16)} (Click for Details)</div>
                </span>
                <Collapse in={open} >
                    <div id="example-fade-text" className="m-4">
                        <strong>Items:</strong><br />
                        {order.products.map(product => {
                        return (
                            <ul key={product.productId}>
                                <li>{product.name} - Quantity: {product.totalAmount}</li>
                            </ul>
                        );
                        })}

                        <strong>Status: </strong>
                        {(order.status === "pending") ?
                            <span className="text-warning">{order.status}</span>
                            :
                            (order.status === "pending cancellation") ?
                                <span className="text-danger">{order.status}</span>   
                                :
                                (order.status === "approved") ?
                                    <span className="text-info">{order.status}</span>
                                    :
                                    <span className="text-success">{order.status}</span>
                        }
                        <br /><br />

                        <Row>
                            <Col lg="9">
                                <strong>Total Price: <span className="text-info h4">&#8369;{order.totalPrice.toLocaleString()}</span></strong>
                            </Col>
                            <Col lg="3" className="text-center">
                                {(order.status !== "delivered") ? 
                                    <CancelOrder status={order.status} userId={''} orderId={order._id} />
                                    :
                                    (order.status === "received") ?
                                        <span></span>
                                        :
                                        <ReceiveOrder orderId={order._id} status={order.status}/>
                                }
                            </Col>
                        </Row>

                                
                    </div>
                </Collapse>
            </div>
        </>
    );
}