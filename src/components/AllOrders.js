import React, { useState } from 'react';
import { Collapse, Row, Col } from 'react-bootstrap';

import ChangeStatus from './ChangeStatus';
import RemoveOrder from './RemoveOrder';


export default function AllOrders({ order, index }) {

    const [open, setOpen] = useState(false);

    return (
        <>
            <div className="border boder-dark">
                <span
                    onClick={() => setOpen(!open)}
                    aria-controls="example-fade-text"
                    aria-expanded={open}
                    className="bg-dark text-light border border-light btn-block rounded-0 text-left"
                > 
                    <div className="m-2">Order #{index + 1} - Purchased on: {order.purchasedOn.slice(4, 16)} (Click for Details)</div>
                </span>
                <Collapse in={open} >
                    <div id="example-fade-text" className="m-4">
                        <span>
                            <strong>Costumer ID: </strong> {order.userId} <br/>
                            <span><strong>Name: </strong>{order.fullName}</span><br/>
                            <span><strong>Username: </strong>{order.username}</span><br/>
                            <span><strong>Email: </strong>{order.email}</span><br/>
                        </span><br/>

                        <strong>Items:</strong><br />
                        
                        {order.products.map(product => {
                        return (
                            <ul key={product.productId}>
                                <li>{product.name} - Quantity: {product.totalAmount}</li>
                            </ul>
                        );
                        })}

                        <strong>Status: </strong>
                        {(order.status === "pending") ?
                            <span className="text-warning">{order.status}</span>
                            :
                            (order.status === "pending cancellation") ?
                                <span className="text-danger">{order.status}</span>   
                                :
                                (order.status === "approved") ?
                                    <span className="text-info">{order.status}</span>
                                    :
                                    <span className="text-success">{order.status}</span>
                        }
                        <br /><br />

                        <Row>
                            <Col lg="9">
                                <strong>Total Price: <span className="text-info h4">&#8369;{order.totalPrice.toLocaleString()}</span></strong>
                            </Col>
                            <Col lg="3" className="text-center form-inline">
                                <ChangeStatus userId={order.userId} orderId={order._id} status={order.status}/>
                                <RemoveOrder userId={order.userId} orderId={order._id}/>
                            </Col>
                        </Row>

                                
                    </div>
                </Collapse>
            </div>
        </>
    );
}