import React from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

import PropTypes from 'prop-types';


export default function Product({ product, productId}) {

    const { name, description, price } = product;

    return (
        <Col lg={6} id="colProduct">
            <Card className="productCard bg-dark text-light" id="cardProduct">
                <Card.Body>
                <Card.Title>
                        <h4><strong>{name}</strong></h4>
                    </Card.Title>
                    <Card.Text id="cardTextProduct">
                        <span><strong>Description:</strong></span>
                        <br />
                        <span>{description}</span>
                        <br />
                        <span><strong>Price:</strong></span>
                        <br />
                        <span>&#8369;{price}</span>
                        <br />
                    </Card.Text>

                </Card.Body>
                
                <Link to={`/products/${productId}`}><Button variant="info" className="btn-block text-bottom rounded-0">Details</Button></Link>
            </Card>
        </Col>
    );
    
}


// Check the property types received by Product components
Product.propTypes = {
    product: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}