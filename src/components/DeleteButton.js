import React from 'react';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function DeleteButton({ productId, isActive }) {
    
    const history = useHistory();

    function deleteProduct() {
        fetch(`https://radiant-garden-68449.herokuapp.com/products/archive/${productId}`, {
            method: 'DELETE',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data === true) {
                Swal.fire({
                    title: "Successful",
                    icon: 'success',
                    text: 'The product is updated'
                })
    
                .then(() => history.go(0));
            }else {
                Swal.fire({
                    title: 'Error',
                    icon: 'error',
                    text: 'Failed to update the product'
                });
            }
        });
    }

    function enableProduct() {
        fetch(`https://radiant-garden-68449.herokuapp.com/products/enable/${productId}`, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data === true) {
                Swal.fire({
                    title: "Successful",
                    icon: 'success',
                    text: 'The product is updated'
                })
    
                .then(() => history.go(0));
            }else {
                Swal.fire({
                    title: 'Error',
                    icon: 'error',
                    text: 'Failed to update the product'
                });
            }
        });
    }
    
    return (
        <>
            {isActive ? 
                <Button variant="outline-danger" className="btn-block" onClick={deleteProduct}>Disable</Button>
                :
                <Button variant="outline-success" className="btn-block" onClick={enableProduct}>Enable</Button>
            }
            
        </>
    );
}