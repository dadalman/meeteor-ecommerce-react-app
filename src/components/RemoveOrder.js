import React, { useEffect, useContext, useState } from 'react';
import Swal from 'sweetalert2';
import { Button } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';

import UserContext from "../UserContext";


export default function RemoveOrder( { userId, orderId, status }) {

    const [disable, setDisable] = useState(true);
    const { user } = useContext(UserContext);
    const history = useHistory();


    useEffect(() => {
        if(user.isAdmin) {
            setDisable(false);
        }else {
            if(status === "pending") {
                setDisable(false);
            }else {
                setDisable(true);
            }
        }
    }, []);

    function removeOrder(userId, orderId) {

        
        console.log(userId, orderId)

        fetch('https://radiant-garden-68449.herokuapp.com/users/removeOrder', {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify({
                userId: userId,
                orderId: orderId
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data) {
                Swal.fire({
                    title: "Order removed!",
                    icon: "success"
                })
                .then(() => history.go(0))
            }else {
                Swal.fire({
                    title: "Something went wrong!",
                    icon: "error",
                    text: "Try to check your internet connection or refresh your page."
                })
            }
        })
    }


    return (
        <>
            <Button variant="danger" onClick={() => removeOrder(userId, orderId)} disabled={disable} className="m-2">Remove</Button>
        </>
    );

}