import React, { useState, useEffect } from 'react';
import { Form, Button, Modal } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom';


export default function UpdateButton({ productId }) {

    const history = useHistory();
    const [show, setShow] = useState(false);

    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState(0);

    const [disable, setDisable] = useState(true);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    useEffect(() => {
        if(name !== '' && description !== '' && price !== 0 && price !== '') {
            setDisable(false);
        }else {
            setDisable(true);
        }
    }, [name, description, price]);

    function updateCourse(e) {
        e.preventDefault();

        fetch(`https://radiant-garden-68449.herokuapp.com/products/update/${productId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data === true) {
                Swal.fire({
                    title: "Successful",
                    icon: 'success',
                    text: 'The product is updated'
                })
    
                .then(() => history.go(0));
            }else {
                Swal.fire({
                    title: 'Error',
                    icon: 'error',
                    text: 'Failed to update the product'
                });
            }
        })
    }

    return (
        <>
            <Button variant="outline-primary" className="btn-block" onClick={handleShow}>Update</Button>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Update Information</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Form>
                        <Form.Group controlId="name">
                            <Form.Label>Product Name:</Form.Label>
                            <Form.Control
                                type="text"
                                value={name}
                                placeholder="Enter new product name"
                                onChange={e => setName(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group controlId="description">
                            <Form.Label>Description:</Form.Label>
                            <Form.Control
                                type="text"
                                value={description}
                                placeholder="Enter new product description"
                                onChange={e => setDescription(e.target.value)}
                                required
                            />
                        </Form.Group>

                        <Form.Group controlId="price">
                            <Form.Label>Price:</Form.Label>
                            <Form.Control
                                type="number"
                                value={price}
                                placeholder="Enter new product price"
                                onChange={e => setPrice(e.target.value)}
                                required
                            />
                        </Form.Group>
                    </Form>
                </Modal.Body>

                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>Close</Button>

                    <Button variant="primary" onClick={e => updateCourse(e)} disabled={disable}>Save Changes</Button>
                </Modal.Footer>
            </Modal>

        
        </>
    );
}