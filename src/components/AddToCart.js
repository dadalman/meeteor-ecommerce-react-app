import React, { useEffect, useState } from 'react';
import Swal from 'sweetalert2';
import { Button } from 'react-bootstrap';

 
export default function AddToCart({ productId, name, price, totalAmount }) {

    const [disable, setDisable] = useState(false);

    totalAmount = Number(totalAmount);

    useEffect(() => {
        if(totalAmount <= 0) {
            setDisable(true);
        }else {
            setDisable(false);
        }
    }, [totalAmount]);

    function addToCart(e) {
        e.preventDefault();

        fetch(`https://radiant-garden-68449.herokuapp.com/users/cart`, {
            method: 'POST',
            'Content-Type': 'application/json',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify({
                productId: productId,
                name: name,
                price: price,
                totalAmount: totalAmount
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            if(data) {
                Swal.fire({
                    title: "Success!",
                    icon: 'success',
                    text: "You successfully added this product to cart"
                });
            }else {
                Swal.fire({
                    title: "Failed",
                    icon: 'error',
                    text: "You failed to add this product to cart"
                });
            }
        })
    }

    return(
        <>
            <Button variant="info" onClick={e => addToCart(e)} disabled={disable}>Add to Cart</Button>
        </>
    );
}