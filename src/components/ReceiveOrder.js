import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom';


export default function ReceiveOrder({ orderId, status }) {

    const history = useHistory();
    const [disable, setDisable] = useState(false);


    useEffect(() => {
        if(status === "received") {
            setDisable(true);
        }else {
            setDisable(false);
        }
    }, []);
    
    function receiveOrder() {
        fetch(fetch(`https://radiant-garden-68449.herokuapp.com/users/status`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            },
            body: JSON.stringify({
                status: "received",
                orderId: orderId
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
    
            if(data) {
                Swal.fire({
                    title: "Order Received!",
                    icon: "success",
                    text: "Thank you for your patronage!"
                })
                .then(() => history.go(0))
            }
        }))
    }


    return (
        <>
            <Button variant="success" onClick={() => receiveOrder()} disabled={disable}>Received</Button>
        </>
    );

}