import React, { Fragment, useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar'
import { Nav } from 'react-bootstrap';
import { Link, NavLink, useHistory } from 'react-router-dom';

import UserContext from '../UserContext';

export default function NavBar() {
    
    const history = useHistory();

    const { user, setUser, unsetUser } = useContext(UserContext);

    const logout = () => {
        unsetUser();
        setUser({accessToken: null});
        history.push('/login');
    }

    let rightNav

    if(user.accessToken !== null && user.isAdmin) {
        rightNav = (
            <Fragment>
                <Nav.Link onClick={logout}>Logout</Nav.Link>
            </Fragment>
        )
    }else if(user.accessToken !== null) {
        rightNav = (
            <Fragment>
                <Nav.Link as={NavLink} to="/user-details">{localStorage.getItem('username')}</Nav.Link>
                <Nav.Link onClick={logout}>Logout</Nav.Link>
            </Fragment>
        )
    }else {
        rightNav = (
            <Fragment>
                <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
            </Fragment>
        )
    }


    return (
        <Navbar bg='light' expand="lg">
            <Navbar.Brand as={Link} to ="/">Meeteor Accessories</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    {(user.isAdmin && user.accessToken !== null) ?
                        <Fragment>
                            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                            <Nav.Link as={NavLink} to="/admin-dashboard">Dashboard</Nav.Link>
                        </Fragment>
                        :
                        (user.accessToken !== null) ?
                            <Fragment>
                                <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                                <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
                                <Nav.Link as={NavLink} to="/cart">Cart</Nav.Link>
                                <Nav.Link as={NavLink} to="/orders">Orders</Nav.Link>
                            </Fragment>
                            :
                            <Fragment>
                                <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                                <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
                            </Fragment>
                    }
                </Nav>
                <Nav className="ml-auto">
                    {rightNav}
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    );
}