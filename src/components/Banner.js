import React from 'react';
import {Jumbotron, Button, Row, Col} from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Banner() {
    return (
        <Row>
            <Col>
                <Jumbotron className="bg-dark text-light">
                    <h1>Meeteor Accessories</h1>
                    <p>Meet your very own crystal straight from the galaxy!</p>
                    <Link to={"/products"}><Button variant="info">Browse Products</Button></Link>
                </Jumbotron>
            </Col>
        </Row>
    )
}