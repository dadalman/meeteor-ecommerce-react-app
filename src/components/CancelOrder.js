import React, { useEffect, useState } from 'react';
import { Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { useHistory } from 'react-router-dom';

import RemoveOrder from './RemoveOrder';


export default function CancelOrder({ status, userId, orderId }) {

    const [disable, setDisable] = useState(false);

    const history = useHistory();


    useEffect(() => {
        if(status !== "approved" && status !== "pending") {
            setDisable(true);
        }else {
            setDisable(false);
        }
    }, [status]);

    function requestCancel() {
        fetch(`https://radiant-garden-68449.herokuapp.com/users/status`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('accessToken')}`
        },
        body: JSON.stringify({
            status: "pending cancellation",
            orderId: orderId
        })
    })
    .then(res => res.json())
    .then(data => {
        console.log(data);

        if(data) {
            Swal.fire({
                title: "Cancel Request Sent",
                icon: "info",
                text: "Your request to cancel your order was sent."
            })
            .then(() => history.go(0))
        }
    })
    }


    return (
        <>
            {(status === "pending") ? 
                <RemoveOrder userId={userId} orderId={orderId} status={status}/>
                :
                <Button variant="danger" onClick={requestCancel} disabled={disable}>Cancel Order</Button>  
             }
            
        </>
    );
}