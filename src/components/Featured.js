import React, { useState, useEffect } from 'react';
import { Row, Col, Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Featured() {

    const [products, setProducts] = useState([]);


    useEffect(() => {
        fetch('https://radiant-garden-68449.herokuapp.com/products/featured', {
            headers: {
                Authorization: `Bearer ${localStorage.getItem('accessToken')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            setProducts(data.map(product => {
                return (
                    <Row key={product._id}>
                        <Col>
                            <Card className="productCard" className="bg-dark text-light m-1">
                                <Card.Body>
                                    <Row>
                                        <Col lg={6}>
                                            <Card.Title>
                                                <h4><strong>{product.name}</strong></h4>
                                            </Card.Title>
                                            <Card.Text>
                                                <span><strong>Description:</strong></span>
                                                <br />
                                                <span>{product.description}</span>
                                                <br />
                                                <span><strong>Price:</strong></span>
                                                <br />
                                                <span>PhP {product.price}</span>
                                                <br />
                                            </Card.Text>

                                            <Link to={`/products/${product._id}`} ><Button variant="info" >Details</Button></Link>
                                        </Col>
                                        <Col lg={6}></Col>
                                    </Row>
                                </Card.Body>
                            </Card>
                        </Col>
                    </Row>
                );
            }))
        })

    }, []);
        

    return (
        <>
            <hr />
            {products}
        </>
    );
}