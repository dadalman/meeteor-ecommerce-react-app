import React, { useState } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';

import './App.css';
import NavBar from './components/NavBar';
import UserContext from './UserContext';

import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Products from './pages/Products'
import AddProduct from './pages/AddProduct';
import ProductDetails from './pages/ProductDetails';
import NotFound from './pages/NotFound';
import Cart from './pages/Cart';
import UserDetails from './pages/UserDetails';
import Orders from './pages/Orders';
import OrdersAdmin from './pages/OrdersAdmin';
import FindUser from './pages/FindUser';
import AdminDashboard from './pages/AdminDashboard';


function App() { 

  const [user, setUser] = useState({
    accessToken: localStorage.getItem('accessToken'),
    fullName: localStorage.getItem('fullName'),
    username: localStorage.getItem('username'),
    email: localStorage.getItem('email'),
    isAdmin: localStorage.getItem('isAdmin') === 'true'
  })

  const [orders, setOrders] = useState([]);

  const unsetUser = () => {
    localStorage.clear();
    setUser({
      accessToken: null,
      isAdmin: null
    });
  }

  return (
    <UserContext.Provider value={{user, setUser, unsetUser, orders, setOrders}}>
      <Router>
        <NavBar />
        <Container>
          <Switch>
            <Route exact path="/register" component={Register} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/add-product" component={AddProduct} />
            <Route exact path="/products/:id" component={ProductDetails} />
            <Route exact path="/products" component={Products} />
            <Route exact path="/cart" component={Cart} />
            <Route exact path="/user-details" component={UserDetails} />
            <Route exact path="/orders" component={Orders} />
            <Route exact path="/orders-admin" component={OrdersAdmin} />
            <Route exact path="/find-user" component={FindUser} />
            <Route exact path="/admin-dashboard" component={AdminDashboard} />
            <Route exact path="/" component={Home} />
            <Route component={NotFound} />
          </Switch>
        </Container>
      </Router>
    </UserContext.Provider>
  );
}

export default App;
